package com.apri.metaweather.utils

import com.apri.metaweather.models.Location
import com.apri.metaweather.models.LocationWeather
import com.apri.metaweather.models.Weather
import org.joda.time.DateTime

object ModelProvider {

    fun location() = Location(123, "Jakarta")

    fun locationWeather() = LocationWeather(listOf(weather(), weather()), "Jakarta")

    fun weather() = Weather("Party Cloudy", "x", DateTime.now(), 12.2)
}