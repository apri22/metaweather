package com.apri.metaweather.viewmodels

import com.apri.metaweather.BaseUnitTest
import com.apri.metaweather.datasource.LocationDao
import com.apri.metaweather.datasource.WeatherAPI
import com.apri.metaweather.models.Location
import com.apri.metaweather.utils.ModelProvider
import io.mockk.*
import io.mockk.impl.annotations.InjectMockKs
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.amshove.kluent.*
import org.junit.Test
import org.koin.test.inject
import retrofit2.Response

@ExperimentalCoroutinesApi
class SearchLocationViewModelTest : BaseUnitTest() {

    @InjectMockKs
    lateinit var viewModel: SearchLocationViewModel

    val weatherAPI: WeatherAPI by inject()
    val locationDao: LocationDao by inject()

    @Test
    fun `find location with keyword on success`() = testDispatcher.runBlockingTest {
        val location = ModelProvider.location()
        val response = mockk<Response<List<Location>>>()
        val args = slot<String>()
        every { response.body() } returns listOf(location)
        coEvery { weatherAPI.searchLocation(capture(args)) } returns response

        viewModel.findLocation("Key")

        coVerify(exactly = 1) { weatherAPI.searchLocation(any()) }
        viewModel.locationNames.value?.shouldContain(location.title)
        viewModel.locationNames.value?.size shouldBeEqualTo 1
        args.captured shouldBeEqualTo "Key"
    }

    @Test
    fun `find location with keyword on error`() = testDispatcher.runBlockingTest {
        val response = mockk<Response<List<Location>>>()
        val args = slot<String>()
        val exception = Exception()
        every { response.body() } throws exception
        coEvery { weatherAPI.searchLocation(capture(args)) } returns response

        viewModel.findLocation("Key")

        coVerify(exactly = 1) { weatherAPI.searchLocation(any()) }
        viewModel.locationNames.value?.shouldBeEmpty()
        viewModel.onError.value shouldBeEqualTo exception
        args.captured shouldBeEqualTo "Key"
    }

    @Test
    fun `save location with valid index`() = testDispatcher.runBlockingTest {
        val location = ModelProvider.location()
        viewModel.locations = listOf(location)
        coEvery { locationDao.insert(any()) } just Runs

        viewModel.saveLocationAtIndex(0).shouldNotBeNull()
        coVerify(exactly = 1) { locationDao.insert(any()) }
    }

    @Test
    fun `save location with invalid index`() = testDispatcher.runBlockingTest {
        coEvery { locationDao.insert(any()) } just Runs

        viewModel.saveLocationAtIndex(0).shouldBeNull()
        coVerify(inverse = true) { locationDao.insert(any()) }
    }
}