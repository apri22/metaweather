package com.apri.metaweather.viewmodels

import com.apri.metaweather.BaseUnitTest
import com.apri.metaweather.datasource.LocationDao
import com.apri.metaweather.models.Location
import com.apri.metaweather.utils.ModelProvider
import io.mockk.Runs
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.just
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.amshove.kluent.shouldBeEmpty
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test
import org.koin.test.inject

@ExperimentalCoroutinesApi
class MainViewModelTest : BaseUnitTest() {

    @InjectMockKs
    lateinit var viewModel: MainViewModel

    val locationDao: LocationDao by inject()

    @Test
    fun `first time state`() {
        viewModel.locationViewModels.value?.shouldBeEmpty()
    }

    @Test
    fun `append new item to location view models`() {
        val location = Location(1, "Jakarta")
        viewModel.addLocationFromResult(location)

        viewModel.locationViewModels.value?.size shouldBeEqualTo 1
        viewModel.locationViewModels.value?.get(0)?.location shouldBeEqualTo location
    }

    @Test
    fun `fetch location from database`() = testDispatcher.runBlockingTest {
        val locations = listOf(Location(123, "Jakarta"))
        coEvery { locationDao.getAll() } returns locations

        viewModel.fetchLocation()

        viewModel.locationViewModels.value?.size shouldBeEqualTo 1
        viewModel.locationViewModels.value?.get(0)?.location shouldBeEqualTo locations.first()
    }

    @Test
    fun `remove location from database`() = testDispatcher.runBlockingTest {
        coEvery { locationDao.delete(any()) } just Runs

        viewModel.removeLocation(ModelProvider.location())

        coVerify(exactly = 1) { locationDao.delete(any()) }
    }


}