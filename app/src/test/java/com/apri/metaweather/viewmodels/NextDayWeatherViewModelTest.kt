package com.apri.metaweather.viewmodels

import com.apri.metaweather.BaseUnitTest
import com.apri.metaweather.utils.ModelProvider
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.SpyK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test

@ExperimentalCoroutinesApi
class NextDayWeatherViewModelTest : BaseUnitTest() {

    @InjectMockKs
    lateinit var viewModel: NextDayWeatherViewModel
    @SpyK
    var weather = ModelProvider.weather()


    @Test
    fun `after initialized`() {
        viewModel.weather shouldBeEqualTo weather
        viewModel.day shouldBeEqualTo weather.date.toString("EEEE")
        viewModel.degree shouldBeEqualTo weather.temp
        viewModel.stateIcon shouldBeEqualTo weather.stateIcon
    }
}