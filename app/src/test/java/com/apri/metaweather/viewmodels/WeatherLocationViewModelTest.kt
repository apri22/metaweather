package com.apri.metaweather.viewmodels

import com.apri.metaweather.BaseUnitTest
import com.apri.metaweather.datasource.WeatherAPI
import com.apri.metaweather.models.Location
import com.apri.metaweather.models.LocationWeather
import com.apri.metaweather.models.Weather
import com.apri.metaweather.utils.ModelProvider
import io.mockk.coEvery
import io.mockk.coVerify
import io.mockk.every
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.SpyK
import io.mockk.mockk
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runBlockingTest
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test
import org.koin.test.inject
import retrofit2.Response

@ExperimentalCoroutinesApi
class WeatherLocationViewModelTest : BaseUnitTest() {

    @InjectMockKs
    lateinit var viewModel: WeatherLocationViewModel

    @SpyK
    var location: Location = Location(122, "Jakarta")
    val weatherAPI: WeatherAPI by inject()


    @Test
    fun `fetching weather when success`() = testDispatcher.runBlockingTest {
        val weather = ModelProvider.weather()
        val model = LocationWeather(listOf(weather))
        val response = mockk<Response<LocationWeather>>()
        every { response.body() } returns model
        coEvery { weatherAPI.weatherByLocationId(any()) } returns response

        viewModel.fetchWeather()

        coVerify(exactly = 1) { weatherAPI.weatherByLocationId(any()) }
        viewModel.weatherText.value shouldBeEqualTo weather.stateName
        viewModel.degreeText.value shouldBeEqualTo weather.temp
        viewModel.stateIconUrl.value shouldBeEqualTo weather.stateIcon
        viewModel.locationText shouldBeEqualTo location.title
    }

    @Test
    fun `fetching weather when failure`() = testDispatcher.runBlockingTest {
        val response = mockk<Response<LocationWeather>>()
        every { response.body() } throws Exception()
        coEvery { weatherAPI.weatherByLocationId(any()) } returns response

        viewModel.fetchWeather()

        coVerify(exactly = 1) { weatherAPI.weatherByLocationId(any()) }
        viewModel.weatherText.value shouldBeEqualTo ""
        viewModel.degreeText.value shouldBeEqualTo ""
        viewModel.locationText shouldBeEqualTo location.title
        viewModel.stateIconUrl.value shouldBeEqualTo ""
    }

    @Test
    fun `fetching weather when weather state already fetched`() = testDispatcher.runBlockingTest {
        viewModel.weatherText.postValue("Test")
        val response = mockk<Response<LocationWeather>>()
        coEvery { weatherAPI.weatherByLocationId(any()) } returns response

        viewModel.fetchWeather()

        coVerify(inverse = true) { weatherAPI.weatherByLocationId(any()) }
        viewModel.weatherText.value shouldBeEqualTo "Test"
        viewModel.degreeText.value shouldBeEqualTo ""
        viewModel.locationText shouldBeEqualTo location.title
        viewModel.stateIconUrl.value shouldBeEqualTo ""
    }
}