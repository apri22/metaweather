package com.apri.metaweather.viewmodels

import com.apri.metaweather.BaseUnitTest
import com.apri.metaweather.utils.ModelProvider
import io.mockk.impl.annotations.InjectMockKs
import io.mockk.impl.annotations.SpyK
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.amshove.kluent.shouldBeEqualTo
import org.amshove.kluent.shouldNotBeNull
import org.junit.Test

@ExperimentalCoroutinesApi
class WeatherPredictionViewModelTest : BaseUnitTest() {

    @InjectMockKs
    lateinit var viewModel: WeatherPredictionViewModel

    @SpyK
    var locationWeather = ModelProvider.locationWeather()

    @Test
    fun `after initialized`() {
        viewModel.locationWeather shouldBeEqualTo locationWeather
        viewModel.todayWeatherViewModel.shouldNotBeNull()
        viewModel.nextWeatherViewModels.size shouldBeEqualTo 1
    }
}