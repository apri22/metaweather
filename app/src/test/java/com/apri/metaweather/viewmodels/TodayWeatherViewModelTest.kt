package com.apri.metaweather.viewmodels

import com.apri.metaweather.BaseUnitTest
import com.apri.metaweather.utils.ModelProvider
import kotlinx.coroutines.ExperimentalCoroutinesApi
import org.amshove.kluent.shouldBeEqualTo
import org.junit.Test

@ExperimentalCoroutinesApi
class TodayWeatherViewModelTest : BaseUnitTest() {

    lateinit var viewModel: TodayWeatherViewModel
    val weather = ModelProvider.weather()
    val city: String = "Jakarta"

    override fun setup() {
        super.setup()
        viewModel = TodayWeatherViewModel(weather, city)
    }

    @Test
    fun `after initialized`() {
        viewModel.weather shouldBeEqualTo weather
        viewModel.city shouldBeEqualTo city

        viewModel.degree shouldBeEqualTo weather.temp
        viewModel.state shouldBeEqualTo weather.stateName
        viewModel.stateIcon shouldBeEqualTo weather.stateIcon
    }
}