package com.apri.metaweather

import android.app.Application
import com.apri.metaweather.injection.apiModule
import com.apri.metaweather.injection.appModule
import com.apri.metaweather.injection.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class MetaWeatherApp : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger(Level.DEBUG)
            androidContext(this@MetaWeatherApp)
            modules(listOf(appModule, viewModelModule, apiModule))
        }
    }
}