package com.apri.metaweather.extension

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer


fun <T> MutableLiveData<List<T>>.postAppending(datas: List<T>?) {
    if (datas == null) return

    if (this.value != null) {
        this.postValue(this.value!! + datas)
    } else {
        this.postValue(datas)
    }
}

fun <T> MutableLiveData<List<T>>.postAppending(data: T?) {
    if (data == null) return

    if (this.value != null) {
        this.postValue(this.value!! + data)
    } else {
        this.postValue(listOf(data))
    }
}


fun <T> MutableLiveData<List<T>>.removeItemAtIndex(index: Int) {
    value.notNull {
        if (index < it.size) {
            val items = it.toMutableList()
            items.removeAt(index)
            postValue(items)
        }
    }
}

fun <T> MutableLiveData<List<T>>.addItemAtIndex(index: Int, data: T) {
    value.notNull {
        if (index < it.size) {
            val items = it.toMutableList()
            items.add(index, data)
            postValue(items)
        }
    }
}


fun <T> LiveData<T>.observe(lifecycleOwner: LifecycleOwner, block: (T) -> Unit) {
    value.notNull { block(it) }
    this.observe(lifecycleOwner, Observer { block(it) })
}

fun <T> T?.notNull(f: (it: T) -> Unit) {
    if (this != null) f(this)
}
