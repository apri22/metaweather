package com.apri.metaweather.viewholders

import android.view.View
import com.apri.metaweather.R
import com.apri.metaweather.base.BaseViewHolder
import com.apri.metaweather.extension.observe
import com.apri.metaweather.viewmodels.IWeatherLocationViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.weather_location_list_item.view.*

class WeatherLocationViewHolder(view: View) : BaseViewHolder(view) {

    fun bindViewModel(viewModel: IWeatherLocationViewModel) {
        itemView.tvLocation.text = viewModel.locationText

        viewModel.degreeText.observe(this) {
            itemView.progressCircular.visibility =
                if (it.isNullOrEmpty()) View.VISIBLE else View.GONE
            itemView.tvDegree.text = it
        }
        viewModel.weatherText.observe(this) {
            itemView.tvWeatherState.text = it
        }
        viewModel.stateIconUrl.observe(this) {
            if (!it.isNullOrEmpty())
                Picasso.get().load(it).into(itemView.ivState)
            else
                itemView.ivState.setImageResource(R.drawable.ic_insert_photo_grey_300_36dp)
        }

        viewModel.fetchWeather()
    }


}