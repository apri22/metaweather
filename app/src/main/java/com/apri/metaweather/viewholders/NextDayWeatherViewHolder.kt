package com.apri.metaweather.viewholders

import android.view.View
import com.apri.metaweather.base.BaseViewHolder
import com.apri.metaweather.viewmodels.INextDayWeatherViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.next_day_weather_list_item.view.*

class NextDayWeatherViewHolder(view: View) : BaseViewHolder(view) {

    fun bindViewModel(viewModel: INextDayWeatherViewModel) {
        itemView.tvDate.text = viewModel.day
        itemView.tvDegree.text = viewModel.degree
        Picasso.get().load(viewModel.stateIcon).into(itemView.ivState)
    }
}