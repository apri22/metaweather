package com.apri.metaweather.viewholders

import android.view.View
import com.apri.metaweather.base.BaseViewHolder
import com.apri.metaweather.viewmodels.ITodayWeatherViewModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.today_weather_list_item.view.*

class TodayWeatherViewHolder(view: View) : BaseViewHolder(view) {

    fun bindViewModel(viewModel: ITodayWeatherViewModel) {
        itemView.tvLocation.text = viewModel.city
        itemView.tvState.text = viewModel.state
        itemView.tvDegree.text = viewModel.degree
        Picasso.get().load(viewModel.stateIcon).into(itemView.ivState)
    }
}