package com.apri.metaweather.common

import com.google.gson.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.ISODateTimeFormat
import java.lang.reflect.Type


class DateTimeConverters : JsonSerializer<DateTime>, JsonDeserializer<DateTime?> {

    private val DATE_FORMATS = arrayOf("yyyy-MM-dd")

    override fun serialize(
        src: DateTime,
        typeOfSrc: Type,
        context: JsonSerializationContext
    ): JsonElement? {
        val dateTimeNoMillisFmt = ISODateTimeFormat.dateTimeNoMillis()
        return JsonPrimitive(dateTimeNoMillisFmt.print(src))
    }

    override fun deserialize(
        json: JsonElement,
        typeOfT: Type,
        context: JsonDeserializationContext
    ): DateTime? {
        for (format in DATE_FORMATS) {
            try {
                val formatter = DateTimeFormat.forPattern(format)
                return formatter.parseDateTime(json.asString)
            } catch (e: IllegalArgumentException) {
                continue
            }
        }
        return null
    }
}