package com.apri.metaweather.common

object ExtraKey {
    const val LOCATION = "location"
    const val LOCATION_WEATHER = "location_weather"
}