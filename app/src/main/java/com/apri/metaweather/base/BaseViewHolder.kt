package com.apri.metaweather.base

import android.view.View
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.recyclerview.widget.RecyclerView

abstract class BaseViewHolder(contentView: View) : RecyclerView.ViewHolder(contentView),
    LifecycleOwner {
    private val lifecycleRegistry = LifecycleRegistry(this)

    init {
        lifecycleRegistry.currentState = Lifecycle.State.INITIALIZED
    }

    fun markAttached() {
        this.lifecycleRegistry.currentState = Lifecycle.State.STARTED
    }

    fun markDetached() {
        this.lifecycleRegistry.currentState = Lifecycle.State.DESTROYED
    }

    override fun getLifecycle(): Lifecycle {
        return this.lifecycleRegistry
    }
}