package com.apri.metaweather.base

import androidx.recyclerview.widget.RecyclerView

abstract class BaseRecyclerAdapter<T : BaseViewHolder> : RecyclerView.Adapter<T>() {

    override fun onBindViewHolder(holder: T, position: Int) {
        holder.markAttached()
    }

    override fun onViewRecycled(holder: T) {
        super.onViewRecycled(holder)
        holder.markDetached()
    }
}