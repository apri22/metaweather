package com.apri.metaweather.viewmodels

import androidx.lifecycle.MutableLiveData
import com.apri.metaweather.base.BaseViewModel
import com.apri.metaweather.datasource.LocationDao
import com.apri.metaweather.datasource.WeatherAPI
import com.apri.metaweather.extension.*
import com.apri.metaweather.models.Location
import org.koin.core.inject

interface ISearchLocationViewModel {
    val locationNames: MutableLiveData<List<String>>
    val onError: MutableLiveData<Exception>


    fun findLocation(keyword: String)
    fun saveLocationAtIndex(index: Int): Location?
}


class SearchLocationViewModel : BaseViewModel(), ISearchLocationViewModel {
    override val locationNames: MutableLiveData<List<String>> = MutableLiveData(listOf())
    override val onError: MutableLiveData<Exception> by lazy { MutableLiveData<Exception>() }

    private val weatherAPI: WeatherAPI by inject()
    private val locationDao: LocationDao by inject()
    var locations = listOf<Location>()

    override fun findLocation(keyword: String) {
        uiJob {
            doAsyncNetworkCall { weatherAPI.searchLocation(keyword) }
                .awaitForResult()
                .withSuccessResult { locations ->
                    this@SearchLocationViewModel.locations = locations
                    locationNames.postValue(locations.map { it.title })
                }.whenFailure {
                    onError.postValue(it)
                }
        }
    }

    override fun saveLocationAtIndex(index: Int): Location? {
        if (index < locations.size) {
            val location = locations[index]
            ioJob { locationDao.insert(location) }
            return location
        }
        return null
    }
}