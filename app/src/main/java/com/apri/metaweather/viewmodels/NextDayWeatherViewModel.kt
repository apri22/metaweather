package com.apri.metaweather.viewmodels

import com.apri.metaweather.base.BaseViewModel
import com.apri.metaweather.models.Weather

interface INextDayWeatherViewModel {
    val day: String
    val stateIcon: String
    val degree: String

    val weather: Weather
}

class NextDayWeatherViewModel(override val weather: Weather) : BaseViewModel(),
    INextDayWeatherViewModel {
    override val day: String = weather.date.toString("EEEE")
    override val stateIcon: String = weather.stateIcon
    override val degree: String = weather.temp
}