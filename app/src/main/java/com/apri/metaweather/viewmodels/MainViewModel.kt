package com.apri.metaweather.viewmodels

import androidx.lifecycle.MutableLiveData
import com.apri.metaweather.base.BaseViewModel
import com.apri.metaweather.common.SingleLiveEvent
import com.apri.metaweather.datasource.LocationDao
import com.apri.metaweather.extension.postAppending
import com.apri.metaweather.extension.uiJob
import com.apri.metaweather.models.Location
import com.apri.metaweather.models.LocationWeather
import org.koin.core.inject

interface IMainViewModel {
    val locationViewModels: MutableLiveData<List<IWeatherLocationViewModel>>
    val onWeatherLocationClicked: SingleLiveEvent<LocationWeather?>

    fun fetchLocation()
    fun addLocationFromResult(location: Location)
    fun removeLocation(location: Location)
}

class MainViewModel : BaseViewModel(), IMainViewModel {

    override val locationViewModels: MutableLiveData<List<IWeatherLocationViewModel>> =
        MutableLiveData()
    override val onWeatherLocationClicked: SingleLiveEvent<LocationWeather?> = SingleLiveEvent()

    val locationDao: LocationDao by inject()

    override fun fetchLocation() {
        uiJob {
            val result = locationDao.getAll()
            locationViewModels.postValue(result.map { WeatherLocationViewModel(it) })
        }
    }

    override fun addLocationFromResult(location: Location) {
        locationViewModels.postAppending(WeatherLocationViewModel(location))
    }

    override fun removeLocation(location: Location) {
        uiJob { locationDao.delete(location) }
    }
}