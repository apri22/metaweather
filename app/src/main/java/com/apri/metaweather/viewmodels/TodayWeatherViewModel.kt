package com.apri.metaweather.viewmodels

import com.apri.metaweather.base.BaseViewModel
import com.apri.metaweather.models.Weather

interface ITodayWeatherViewModel {
    val city: String
    val degree: String
    val state: String
    val stateIcon: String

    val weather: Weather
}

class TodayWeatherViewModel(
    override val weather: Weather,
    override val city: String
) : BaseViewModel(), ITodayWeatherViewModel {
    override val degree: String = weather.temp
    override val state: String = weather.stateName
    override val stateIcon: String = weather.stateIcon
}