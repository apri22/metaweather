package com.apri.metaweather.viewmodels

import com.apri.metaweather.base.BaseViewModel
import com.apri.metaweather.models.LocationWeather

interface IWeatherPredictionViewModel {
    val locationWeather: LocationWeather

    val todayWeatherViewModel: ITodayWeatherViewModel
    val nextWeatherViewModels: List<INextDayWeatherViewModel>
}

class WeatherPredictionViewModel(override val locationWeather: LocationWeather) : BaseViewModel(),
    IWeatherPredictionViewModel {
    override val todayWeatherViewModel: ITodayWeatherViewModel =
        TodayWeatherViewModel(locationWeather.weathers.first(), locationWeather.location)
    override val nextWeatherViewModels: List<INextDayWeatherViewModel> =
        locationWeather.weathers.slice(1 until locationWeather.weathers.size)
            .map { NextDayWeatherViewModel(it) }
}