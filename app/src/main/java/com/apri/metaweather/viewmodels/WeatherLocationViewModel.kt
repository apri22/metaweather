package com.apri.metaweather.viewmodels

import androidx.lifecycle.MutableLiveData
import com.apri.metaweather.base.BaseViewModel
import com.apri.metaweather.datasource.WeatherAPI
import com.apri.metaweather.extension.awaitForResult
import com.apri.metaweather.extension.doAsyncNetworkCall
import com.apri.metaweather.extension.uiJob
import com.apri.metaweather.extension.withSuccessResult
import com.apri.metaweather.models.Location
import com.apri.metaweather.models.LocationWeather
import org.koin.core.inject

interface IWeatherLocationViewModel {
    val locationText: String
    val weatherText: MutableLiveData<String>
    val location: Location
    val degreeText: MutableLiveData<String>
    val stateIconUrl: MutableLiveData<String>
    var locationWeather: LocationWeather?

    fun fetchWeather()
}

class WeatherLocationViewModel(override val location: Location) : BaseViewModel(),
    IWeatherLocationViewModel {
    override val locationText: String = location.title
    override val weatherText: MutableLiveData<String> by lazy { MutableLiveData("") }
    override val degreeText: MutableLiveData<String> by lazy { MutableLiveData("") }
    override val stateIconUrl: MutableLiveData<String> by lazy { MutableLiveData("") }

    override var locationWeather: LocationWeather? = null

    private val weatherAPI: WeatherAPI by inject()

    override fun fetchWeather() {
        if (!weatherText.value.isNullOrEmpty()) return
        uiJob {
            doAsyncNetworkCall { weatherAPI.weatherByLocationId(location.id) }
                .awaitForResult()
                .withSuccessResult {
                    locationWeather = it
                    weatherText.postValue(it.weathers.firstOrNull()?.stateName)
                    degreeText.postValue(it.weathers.firstOrNull()?.temp)
                    stateIconUrl.postValue(it.weathers.firstOrNull()?.stateIcon)
                }
        }
    }
}