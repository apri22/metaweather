package com.apri.metaweather.pages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import com.apri.metaweather.R
import com.apri.metaweather.base.BaseActivity
import com.apri.metaweather.base.BaseRecyclerAdapter
import com.apri.metaweather.base.BaseViewHolder
import com.apri.metaweather.common.ExtraKey
import com.apri.metaweather.viewholders.NextDayWeatherViewHolder
import com.apri.metaweather.viewholders.TodayWeatherViewHolder
import com.apri.metaweather.viewmodels.IWeatherPredictionViewModel
import com.apri.metaweather.viewmodels.WeatherPredictionViewModel
import kotlinx.android.synthetic.main.activity_weather_prediction.*
import org.koin.android.ext.android.inject
import org.koin.core.parameter.parametersOf

class WeatherPredictionActivity : BaseActivity() {

    private val viewModel: IWeatherPredictionViewModel by inject<WeatherPredictionViewModel>{
        parametersOf(intent.extras?.getParcelable(ExtraKey.LOCATION_WEATHER))
    }
    private val adapter by lazy { WeatherPredictionAdapter(viewModel) }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_weather_prediction)

        this.setupToolbar()
        this.setupRecyclerView()
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        supportActionBar?.title = ""
        toolbar.setNavigationOnClickListener { finish() }
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
    }
}

class WeatherPredictionAdapter(val viewModel: IWeatherPredictionViewModel) :
    BaseRecyclerAdapter<BaseViewHolder>() {
    private val todayType = 0
    private val nextType = 1

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BaseViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return if (viewType == todayType) {
            val view = inflater.inflate(R.layout.today_weather_list_item, parent, false)
            TodayWeatherViewHolder(view)
        } else {
            val view = inflater.inflate(R.layout.next_day_weather_list_item, parent, false)
            NextDayWeatherViewHolder(view)
        }
    }

    override fun getItemCount(): Int {
        return 1 + viewModel.nextWeatherViewModels.size
    }

    override fun onBindViewHolder(holder: BaseViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        if (holder is TodayWeatherViewHolder) {
            holder.bindViewModel(viewModel.todayWeatherViewModel)
        } else if (holder is NextDayWeatherViewHolder) {
            holder.bindViewModel(viewModel.nextWeatherViewModels[position - 1])
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) {
            todayType
        } else {
            nextType
        }
    }

}