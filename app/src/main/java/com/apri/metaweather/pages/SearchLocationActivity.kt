package com.apri.metaweather.pages

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.lifecycle.Observer
import com.apri.metaweather.R
import com.apri.metaweather.base.BaseActivity
import com.apri.metaweather.common.ExtraKey
import com.apri.metaweather.extension.launchMain
import com.apri.metaweather.extension.notNull
import com.apri.metaweather.viewmodels.ISearchLocationViewModel
import com.apri.metaweather.viewmodels.SearchLocationViewModel
import kotlinx.android.synthetic.main.activity_search_location.*
import kotlinx.coroutines.delay
import org.koin.android.ext.android.inject

class SearchLocationActivity : BaseActivity() {
    companion object {
        const val SEARCH_LOCATION_REQUEST_CODE = 123
    }

    private val viewModel: ISearchLocationViewModel by inject<SearchLocationViewModel>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_location)
        this.setupToolbar()
        this.setupRecyclerView()
        this.setupSearchView()

        viewModel.onError.observe(this, Observer {
            Toast.makeText(this, R.string.err_something_when_wrong, Toast.LENGTH_SHORT).show()
        })

        viewModel.locationNames.observe(this, Observer {
            val locationAdapter =
                ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, it)
            listView.adapter = locationAdapter
        })
    }

    private fun setupToolbar() {
        setSupportActionBar(toolbar)
        toolbar.setNavigationOnClickListener { finish() }
    }

    private fun setupRecyclerView() {
        listView.setOnItemClickListener { _, _, index, _ ->
            viewModel.saveLocationAtIndex(index).notNull {
                val intent = Intent()
                intent.putExtra(ExtraKey.LOCATION, it)
                setResult(Activity.RESULT_OK, intent)
                finish()
            }
        }
    }

    private fun setupSearchView() {
        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            private var searchFor = ""
            override fun onQueryTextSubmit(query: String?): Boolean {
                doSearch(query)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                doSearch(newText)
                return true
            }

            private fun doSearch(keyword: String?) {
                searchFor = keyword ?: ""
                launchMain {
                    delay(250)
                    if (keyword != searchFor) return@launchMain
                    viewModel.findLocation(keyword)
                }
            }

        })
    }
}
