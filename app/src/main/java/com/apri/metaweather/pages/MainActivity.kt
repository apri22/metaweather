package com.apri.metaweather.pages

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.apri.metaweather.R
import com.apri.metaweather.base.BaseActivity
import com.apri.metaweather.base.BaseRecyclerAdapter
import com.apri.metaweather.common.ExtraKey
import com.apri.metaweather.common.SingleLiveEvent
import com.apri.metaweather.extension.addItemAtIndex
import com.apri.metaweather.extension.notNull
import com.apri.metaweather.extension.removeItemAtIndex
import com.apri.metaweather.models.Location
import com.apri.metaweather.models.LocationWeather
import com.apri.metaweather.viewholders.WeatherLocationViewHolder
import com.apri.metaweather.viewmodels.IMainViewModel
import com.apri.metaweather.viewmodels.IWeatherLocationViewModel
import com.apri.metaweather.viewmodels.MainViewModel
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.weather_location_list_item.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class MainActivity : BaseActivity() {

    val viewModel: IMainViewModel by viewModel<MainViewModel>()

    private val adapter: WeatherLocationAdapter by lazy { WeatherLocationAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        this.setupRecyclerView()
        this.setupTouchHelper()

        viewModel.locationViewModels.observe(this, Observer {
            adapter.items = it
            adapter.notifyDataSetChanged()
        })
        viewModel.onWeatherLocationClicked.observe(this, Observer {
            it.notNull { data ->
                val intent = Intent(this, WeatherPredictionActivity::class.java)
                intent.putExtra(ExtraKey.LOCATION_WEATHER, data)
                startActivity(intent)
            }
        })

        viewModel.fetchLocation()

        fabAddLocation.setOnClickListener {
            val intent = Intent(this, SearchLocationActivity::class.java)
            startActivityForResult(intent, SearchLocationActivity.SEARCH_LOCATION_REQUEST_CODE)
        }
    }

    private fun setupRecyclerView() {
        val layoutManager = LinearLayoutManager(this)
        recyclerView.itemAnimator = DefaultItemAnimator()
        recyclerView.layoutManager = layoutManager
        recyclerView.adapter = adapter
        adapter.itemClick = viewModel.onWeatherLocationClicked

    }

    private fun setupTouchHelper() {
        val itemTouchCallback = object : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
            override fun onMove(
                recyclerView: RecyclerView,
                viewHolder: RecyclerView.ViewHolder,
                target: RecyclerView.ViewHolder
            ): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val item = viewModel.locationViewModels.value?.get(position)
                viewModel.locationViewModels.removeItemAtIndex(position)
                val snackbar = Snackbar.make(
                    viewHolder.itemView,
                    getString(R.string.item_removed_label),
                    Snackbar.LENGTH_LONG
                )
                snackbar.addCallback(object : Snackbar.Callback() {
                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        if (event == DISMISS_EVENT_TIMEOUT || event == DISMISS_EVENT_CONSECUTIVE) {
                            item?.location.notNull { viewModel.removeLocation(it) }
                        }
                    }
                })
                snackbar.setAction(getString(R.string.cancel_label)) {
                    item.notNull { vm ->
                        viewModel.locationViewModels.addItemAtIndex(position, vm)
                    }
                }
                snackbar.show()

            }
        }
        ItemTouchHelper(itemTouchCallback).attachToRecyclerView(recyclerView)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SearchLocationActivity.SEARCH_LOCATION_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            data?.extras?.getParcelable<Location>(ExtraKey.LOCATION).notNull {
                viewModel.addLocationFromResult(it)
            }
        }
    }
}

private class WeatherLocationAdapter : BaseRecyclerAdapter<WeatherLocationViewHolder>() {

    var items = listOf<IWeatherLocationViewModel>()
    var itemClick: SingleLiveEvent<LocationWeather?>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): WeatherLocationViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.weather_location_list_item, parent, false)
        return WeatherLocationViewHolder(view)
    }

    override fun getItemCount(): Int {
        return items.count()
    }

    override fun onBindViewHolder(holder: WeatherLocationViewHolder, position: Int) {
        super.onBindViewHolder(holder, position)
        val item = items[position]
        holder.bindViewModel(items[position])
        holder.itemView.cardView.setOnClickListener { itemClick?.postValue(item.locationWeather) }
    }
}