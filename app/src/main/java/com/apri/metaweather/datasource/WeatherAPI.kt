package com.apri.metaweather.datasource

import com.apri.metaweather.models.Location
import com.apri.metaweather.models.LocationWeather
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface WeatherAPI {

    @GET("api/location/search")
    suspend fun searchLocation(@Query("query") keyword: String): Response<List<Location>>

    @GET("api/location/{id}")
    suspend fun weatherByLocationId(@Path("id") id: Int): Response<LocationWeather>
}