package com.apri.metaweather.datasource

import androidx.room.*
import com.apri.metaweather.models.Location
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import java.lang.reflect.Type


@Database(entities = [Location::class], version = 1)
abstract class WeatherDatabase : RoomDatabase() {
    abstract fun locationDao(): LocationDao
}

@Dao
interface LocationDao {
    @Query("select * from location")
    suspend fun getAll(): List<Location>

    @Insert(onConflict = OnConflictStrategy.ABORT)
    suspend fun insert(location: Location)

    @Delete
    suspend fun delete(location: Location)
}
