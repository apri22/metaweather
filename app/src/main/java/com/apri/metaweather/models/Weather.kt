package com.apri.metaweather.models

import android.os.Parcelable
import com.apri.metaweather.injection.BASE_URL
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import org.joda.time.DateTime

@Parcelize
data class Weather(
    @SerializedName("weather_state_name")
    val stateName: String,
    @SerializedName("weather_state_abbr")
    val _stateIcon: String,
    @SerializedName("applicable_date")
    val date: DateTime,
    @SerializedName("the_temp")
    val _temp: Double
) : Parcelable {
    val stateIcon: String get() = "$BASE_URL/static/img/weather/png/64/$_stateIcon.png"
    val temp: String get() = "${_temp.toInt()}°C"
}