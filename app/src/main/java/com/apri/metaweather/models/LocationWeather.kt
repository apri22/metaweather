package com.apri.metaweather.models

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LocationWeather(
    @SerializedName("consolidated_weather") val weathers: List<Weather>,
    @SerializedName("title") val location: String = ""
) : Parcelable