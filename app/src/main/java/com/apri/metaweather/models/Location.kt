package com.apri.metaweather.models

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Entity
@Parcelize
data class Location(
    @PrimaryKey
    @SerializedName("woeid") val id: Int,
    @ColumnInfo(name = "title") val title: String
) : Parcelable