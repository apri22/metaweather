package com.apri.metaweather.injection

import androidx.room.Room
import com.apri.metaweather.common.DateTimeConverters
import com.apri.metaweather.datasource.WeatherAPI
import com.apri.metaweather.datasource.WeatherDatabase
import com.apri.metaweather.models.LocationWeather
import com.apri.metaweather.viewmodels.MainViewModel
import com.apri.metaweather.viewmodels.SearchLocationViewModel
import com.apri.metaweather.viewmodels.WeatherPredictionViewModel
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.joda.time.DateTime
import org.koin.android.ext.koin.androidApplication
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

val appModule = module {
    single {
        Room.databaseBuilder(
            androidApplication(),
            WeatherDatabase::class.java,
            "weather-database"
        ).build()
    }

    single {
        get<WeatherDatabase>().locationDao()
    }
}

val viewModelModule = module {
    viewModel { MainViewModel() }
    viewModel { SearchLocationViewModel() }
    viewModel { (locationWeather: LocationWeather) -> WeatherPredictionViewModel(locationWeather) }
}


val BASE_URL = "https://www.metaweather.com/"

val apiModule = module {
    single {
        OkHttpClient.Builder()
            .connectTimeout(60, TimeUnit.SECONDS)
            .readTimeout(60, TimeUnit.SECONDS)
            .addInterceptor(HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
            .build()
    }
    single {
        val type = object : TypeToken<DateTime>() {}.type
        GsonBuilder().registerTypeAdapter(type, DateTimeConverters()).create()
    }

    single {
        Retrofit.Builder()
            .client(get())
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(get()))
            .build()
    }
    single { get<Retrofit>().create(WeatherAPI::class.java) }
}
