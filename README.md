# Meta Weather App
<p align="center"><img src="screenshot/ss1.png" width="200" />&nbsp;&nbsp;&nbsp;<img src="screenshot/ss2.png" width="200" />&nbsp;&nbsp;&nbsp;<img src="screenshot/ss3.png" width="200" /></p>
---

**Please use develop branch**

To create development build just run:
`./gradlew assembleDebug`
it will produce unsigned APK in `build/output/apk/debug` folder

Architecture Pattern: **MVVM**

### Library used:
* [ViewModelComponent](https://developer.android.com/topic/libraries/architecture/viewmodel) (Lifecycle aware view model)
* [OKHttp](https://github.com/square/okhttp) (HTTP Client)
* [Retrofit](https://github.com/square/retrofit) (HTTP Client)
* [Picasso](https://github.com/square/picasso) (Image Loader)
* [Coroutine](https://developer.android.com/kotlin/coroutines) (Threading like a boss)
* [Koin](https://insert-koin.io/) (Dependency Injection)
* [Room](https://developer.android.com/topic/libraries/architecture/room) (Local Database)

### Testing Library Used
* [Jacoco](https://www.jacoco.org/) (Code coverage library)
* [JUnit4](https://github.com/junit-team/junit4) (Testing Framework)
* [Mockk](https://github.com/mockk/mockk) (Mocking library for kotlin)
* [Kluent](https://github.com/MarkusAmshove/Kluent) ( Assertion library )


## Generate Test coverage
Open your terminal, go to your root project directory, then run this command:
`./gradlew clean assembleDebug testDebugUnitTest jacocoTestReport`

You can see test coverage report in:
`$rootProject/$yourModule/build/reports/jacoco/jacocoTestReport/html/`

---
## How to use the app
- click floating action button to search location
- type city name on search location page
- click on the city result
- then it will added to weather list page
- you can click item in weather list page to go to weather prediction page
- remove weather list by swiping to the left


---
